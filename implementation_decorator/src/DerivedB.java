public class DerivedB extends Base {
    @Override
    protected String actionDescription() {
        return "ActionB";
    }

    @Override
    protected char action() {
        return 'B';
    }
}
