public class DerivedA extends Base {
    @Override
    protected String actionDescription() {
        return "ActionA";
    }

    @Override
    protected char action() {
        return 'A';
    }
}
