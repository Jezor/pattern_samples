public class Main {
    public static void main(String[] args) {
        final Base derivedA = new DerivedA();
        final Base derivedB = new DerivedB();

        derivedA.performAction();
        derivedB.performAction();
    }
}
