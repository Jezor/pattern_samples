import static java.lang.System.out;

public abstract class Base {

    protected abstract String actionDescription();

    protected abstract char action();

    public char performAction() {
        out.println("Performing action " + actionDescription());
        final char actionResult = action();
        out.println("Action " + actionDescription() + " result is " + actionResult);
        return actionResult;
    }
}
